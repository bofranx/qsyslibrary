﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class EngineStatusEventArgs : EventArgs
    {
        public string State { get; set; }
        public string DesignName { get; set; }
        public string DesignCode { get; set; }
        public ushort Initialized { get; set; }

        public EngineStatusEventArgs()
        {

        }

        public EngineStatusEventArgs(bool initialized)
        {
            this.Initialized = (ushort)(initialized == true ? 1 : 0);
        }

        public EngineStatusEventArgs(string state, string designName, string designCode, bool initialized)
        {
            this.State = state;

            this.DesignName = designName;

            this.DesignCode = designCode;

            this.Initialized = (ushort)(initialized == true ? 1 : 0);
        }
    }
}