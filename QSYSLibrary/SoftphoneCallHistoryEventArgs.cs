﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class SoftphoneCallHistoryEventArgs
    {
        public List<CallHistoryItem> CallHistory = new List<CallHistoryItem>();
    }

    public class CallHistoryItem
    {
        public int CallID { get; set; }
        public string Timestamp { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public bool Incoming { get; set; }
        public bool Answered { get; set; }

    }
}