﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QSYSLibrary
{
    public class GainComponent
    {
        Component component = new Component();

        public string componentName;
        internal QSYSClient myClient;
        internal JsonSerializerSettings jsonSettings;

        //delegates
        public delegate void AnalogDelegate(ushort a);
        public delegate void SignedAnalogDelegate(short a);
        public delegate void SerialDelegate(SimplSharpString s);

        public SignedAnalogDelegate gainValueDb { get; set; }
        public AnalogDelegate gainPercentage { get; set; }
        public AnalogDelegate gainLogPercentage { get; set; }
        public AnalogDelegate muteValue { get; set; }

        private short _gainDbValue;
        public short GainDbValue
        {
            get
            {
                return _gainDbValue;
            }
            set
            {
                if (myClient != null && _gainDbValue != value)
                {
                    MethodComponentSet componentChange = new MethodComponentSet();
                    componentChange.@params.Name = this.componentName;
                    MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
                    control.Name = "gain";
                    control.Value = (decimal)value;

                    componentChange.@params.Controls.Add(control);
                    this.myClient.SendData(JsonConvert.SerializeObject(componentChange, Formatting.None, jsonSettings));

                    this._gainDbValue = value;
                }
            }
        }

        private ushort _gainPercentValue;
        public ushort GainPercentValue
        {
            get
            {
                return _gainPercentValue;
            }
            set
            {
                if (myClient != null && _gainPercentValue != value)
                {
                    this.SetGainPercentageValue(value);
                    _gainPercentValue = value;
                }
            }
        }

        private ushort _gainLogValue;
        public ushort GainLogValue
        {
            get
            {
                return _gainLogValue;
            }
            set
            {
                if (myClient != null && _gainLogValue != value)
                {
                    this.SetGainLogValue(value);
                    _gainLogValue = value;
                }
            }
        }



        /// <summary>
        /// Attempt to add this component to the Components dictionary of QSYSClient
        /// </summary>
        /// <param name="name">Component name</param>
        /// <param name="id">Core ID</param>
        public void Register(string name, ushort id)
        {
            this.componentName = name;

            component.Name = this.componentName;
            List<ControlName> controls = new List<ControlName>();
            controls.Add(new ControlName("gain"));
            controls.Add(new ControlName("mute"));
            component.Controls = controls;

            if (QSYSMain.Devices.ContainsKey(id))
            {
                this.myClient = QSYSMain.Devices[id];

                if (this.myClient.RegisterNamedComponent(component))
                {
                    this.myClient.Components[component].OnNewEvent += new EventHandler<QsysInternalEventArgs>(Component_OnNewEvent);
                }

            }
            else
            {
                ErrorLog.Error("QSYSLibrary NamedComponent({0}): client is invalid", this.componentName);
            }
        }

        private void Component_OnNewEvent(object sender, QsysInternalEventArgs e)
        {

            //for testing only
            //if (newTestData != null)
            //    newTestData(e.Name, e.String);

            switch (e.Name)
            {
                case "gain":
                    if (gainPercentage != null)
                    {
                        _gainPercentValue = (ushort)(e.Position * 65535.00);
                        gainPercentage(_gainPercentValue);
                    }

                    if (gainValueDb != null)
                    {
                        _gainDbValue = (short)e.Value;
                        gainValueDb(_gainDbValue);
                    }

                    if (gainLogPercentage != null)
                    {
                        if (e.Value > 0)
                        {
                            _gainLogValue = 65535;
                            gainLogPercentage(_gainLogValue);
                        }
                        else
                        {
                            _gainLogValue = Helpers.logScaleReturnValue(e.Value);
                            gainLogPercentage(_gainLogValue);
                        }
                    }

                    break;

                case "mute":
                    if (muteValue != null)
                        muteValue((ushort)e.Value);

                    break;
            }
        }        

        public void SetMute(ushort value)
        {
            MethodComponentSet componentChange = new MethodComponentSet();
            componentChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "mute";
            control.Value = value;

            componentChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(componentChange));
        }

        public void SetGainLogValue(ushort value)
        {
            MethodComponentSet componentChange = new MethodComponentSet();
            componentChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "gain";
            control.Value = (decimal)Helpers.logSetFaderValue(value);

            componentChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(componentChange, Formatting.None, jsonSettings));
        }

        public void SetGainPercentageValue(ushort value)
        {
            MethodComponentSet componentChange = new MethodComponentSet();
            componentChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "gain";
            control.Position = (double)(value / 65535.00);

            componentChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(componentChange, Formatting.None, jsonSettings));
        }

        public GainComponent()
        {
            this.jsonSettings = new JsonSerializerSettings();
            jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            jsonSettings.NullValueHandling = NullValueHandling.Ignore;
        }

    }
}