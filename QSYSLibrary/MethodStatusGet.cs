﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public class MethodStatusGet : MethodBase
    {
        public int @params = 0;
        public MethodStatusGet()
        {
            this.id = 1;
            this.method = "StatusGet";
        }
    }
}