﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public class MethodControlSetParams
    {
        public string Name { get; set; }

        public string String { get; set; }

        public decimal? Value { get; set; }

        public double? Position { get; set; }

        public double? Ramp { get; set; }

        public MethodControlSetParams()
        {
        }
    }
}