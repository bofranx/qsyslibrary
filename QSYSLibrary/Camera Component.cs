﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QSYSLibrary
{
    public class CameraComponent 
    {
        Component component = new Component();

        public string componentName;
        internal QSYSClient myClient;

        //delegates
        public delegate void AnalogDelegate(ushort a);
        public delegate void SerialDelegate(SimplSharpString s);

        public SerialDelegate ptzPreset { get; set; }
        public SerialDelegate panSpeed { get; set; }
        public SerialDelegate tiltSpeed { get; set; }
        public SerialDelegate zoomSpeed { get; set; }
        public SerialDelegate snapshotSpeed { get; set; }
        public SerialDelegate urlStream1Display { get; set; }
        public SerialDelegate urlStream2Display { get; set; }
        public AnalogDelegate focusAuto { get; set; }
        public AnalogDelegate togglePrivacy { get; set; }
        public AnalogDelegate presetHomeLoad { get; set; }

        

        /// <summary>
        /// Attempt to add this component to the Components dictionary of QSYSClient
        /// </summary>
        /// <param name="name">Component name</param>
        /// <param name="id">Core ID</param>
        public void Register(string name, ushort id)
        {
            this.componentName = name;

            component.Name = this.componentName;
            List<ControlName> controls = new List<ControlName>();
            controls.Add(new ControlName("ptz.preset"));
            controls.Add(new ControlName("setup.pan.speed"));
            controls.Add(new ControlName("setup.tilt.speed"));
            controls.Add(new ControlName("setup.zoom.speed"));
            controls.Add(new ControlName("aaaa.setup.snapshot.speed"));
            controls.Add(new ControlName("focus.auto"));
            controls.Add(new ControlName("toggle.privacy"));
            controls.Add(new ControlName("preset.home.load"));
            controls.Add(new ControlName("url.stream.1.display"));
            controls.Add(new ControlName("url.stream.2.display"));
            component.Controls = controls;

            if (QSYSMain.Devices.ContainsKey(id))
            {
                this.myClient = QSYSMain.Devices[id];

                if (this.myClient.RegisterNamedComponent(component))
                {
                    this.myClient.Components[component].OnNewEvent += new EventHandler<QsysInternalEventArgs>(Component_OnNewEvent);
                }

            }
            else
            {
                ErrorLog.Error("QSYSLibrary NamedComponent({0}): client is invalid", this.componentName);
            }
        }

        private void Component_OnNewEvent(object sender, QsysInternalEventArgs e)
        {

            //for testing only
            //if (newTestData != null)
            //    newTestData(e.Name, e.String);

            switch (e.Name)
            {
                case "ptz.preset":
                    if (ptzPreset != null)
                        ptzPreset(e.String);
                    break;

                case "setup.pan.speed":
                    if (panSpeed != null)
                        panSpeed(e.String);
                    break;

                case "setup.tilt.speed":
                    if (tiltSpeed != null)
                        tiltSpeed(e.String);
                    break;

                case "setup.zoom.speed":
                    if (zoomSpeed != null)
                        zoomSpeed(e.String);
                    break;

                case "aaaa.setup.snapshot.speed":
                    if (snapshotSpeed != null)
                        snapshotSpeed(e.String);
                    break;

                case "url.stream.1.display":
                    if (urlStream1Display != null)
                        urlStream1Display(e.String);
                    break;

                case "url.stream.2.display":
                    if (urlStream2Display != null)
                        urlStream2Display(e.String);
                    break;

                case "focus.auto":
                    if (focusAuto != null)
                        focusAuto((ushort)e.Value);
                    break;

                case "toggle.privacy":
                    if (togglePrivacy != null)
                        togglePrivacy((ushort)e.Value);
                    break;

                case "preset.home.load":
                    if (presetHomeLoad != null)
                        presetHomeLoad((ushort)e.Value);
                    break;
            }
        }

        public void SetPtzPosition(SimplSharpString ptzPosition)
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            var control = new { Name = "ptz.preset", Value = ptzPosition.ToString() };            

            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public void SetFocusAuto()
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "focus.auto";
            control.Value = 1;

            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public void RecallHome()
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "preset.home.load";
            control.Value = 1;

            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public void SaveHome()
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "preset.home.save.trigger";
            control.Value = 1;

            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public void SetPrivacy(ushort value)
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = "toggle.privacy";
            control.Value = value;

            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public void StartPTZ(ushort type)
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            switch ((ePtzTypes)type)
            {
                case ePtzTypes.Up:
                    control.Name = "tilt.up";
                    control.Value = 1;
                    break;
                case ePtzTypes.Down:
                    control.Name = "tilt.down";
                    control.Value = 1;
                    break;
                case ePtzTypes.Left:
                    control.Name = "pan.left";
                    control.Value = 1;
                    break;
                case ePtzTypes.Right:
                    control.Name = "pan.right";
                    control.Value = 1;
                    break;
                case ePtzTypes.ZoomIn:
                    control.Name = "zoom.in";
                    control.Value = 1;
                    break;
                case ePtzTypes.ZoomOut:
                    control.Name = "zoom.out";
                    control.Value = 1;
                    break;
                case ePtzTypes.FocusFar:
                    control.Name = "focus.far";
                    control.Value = 1;
                    break;
                case ePtzTypes.FocusNear:
                    control.Name = "focus.near";
                    control.Value = 1;
                    break;
                default:
                    break;
            }
            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public void StopPTZ(ushort type)
        {
            MethodComponentSet cameraChange = new MethodComponentSet();
            cameraChange.@params.Name = this.componentName;
            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            switch ((ePtzTypes)type)
            {
                case ePtzTypes.Up:
                    control.Name = "tilt.up";
                    control.Value = 0;
                    break;
                case ePtzTypes.Down:
                    control.Name = "tilt.down";
                    control.Value = 0;
                    break;
                case ePtzTypes.Left:
                    control.Name = "pan.left";
                    control.Value = 0;
                    break;
                case ePtzTypes.Right:
                    control.Name = "pan.right";
                    control.Value = 0;
                    break;
                case ePtzTypes.ZoomIn:
                    control.Name = "zoom.in";
                    control.Value = 0;
                    break;
                case ePtzTypes.ZoomOut:
                    control.Name = "zoom.out";
                    control.Value = 0;
                    break;
                case ePtzTypes.FocusFar:
                    control.Name = "focus.far";
                    control.Value = 0;
                    break;
                case ePtzTypes.FocusNear:
                    control.Name = "focus.near";
                    control.Value = 0;
                    break;
                default:
                    break;
            }
            cameraChange.@params.Controls.Add(control);
            this.myClient.SendData(JsonConvert.SerializeObject(cameraChange));
        }

        public enum ePtzTypes
        {
            Up = 1,
            Down = 2,
            Left = 3,
            Right = 4,
            ZoomIn = 5,
            ZoomOut = 6,
            FocusFar = 7,
            FocusNear = 8
        }
    }
}