﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QSYSLibrary
{
    public class SignalPresenceComponent
    {
        Component component = new Component();

        public string componentName;
        internal QSYSClient myClient;

        //delegates
        public delegate void SignalPresenceValue(ushort index, ushort val);

        public SignalPresenceValue OnSignalPresence { get; set; }

        /// <summary>
        /// Attempt to add this component to the Components dictionary of QSYSClient
        /// </summary>
        /// <param name="name">Component name</param>
        /// <param name="id">Core ID</param>
        public void Register(string name, ushort id, ushort channelCount)
        {
            this.componentName = name;

            component.Name = this.componentName;
            List<ControlName> controls = new List<ControlName>();

            if(channelCount <= 1)
                controls.Add(new ControlName("signal.presence"));
            else
            {
                for (int i = 1; i <= channelCount; i++)
                {
                    controls.Add(new ControlName("signal.presence." + i.ToString()));
                }
            }
            component.Controls = controls;

            if (QSYSMain.Devices.ContainsKey(id))
            {
                this.myClient = QSYSMain.Devices[id];

                if (this.myClient.RegisterNamedComponent(component))
                {
                    this.myClient.Components[component].OnNewEvent += new EventHandler<QsysInternalEventArgs>(Component_OnNewEvent);
                }

            }
            else
            {
                ErrorLog.Error("QSYSLibrary NamedComponent({0}): client is invalid", this.componentName);
            }
        }

        private void Component_OnNewEvent(object sender, QsysInternalEventArgs e)
        {

            //for testing only
            //if (newTestData != null)
            //    newTestData(e.Name, e.String);

            if (e.Name.Contains("signal.presence"))
            {
                string[] array = e.Name.Split('.');

                if (array.Count() <= 2)
                {
                    OnSignalPresence(1, (ushort)e.Value);
                }
                else
                {
                    OnSignalPresence(Convert.ToUInt16(array[2]), (ushort)e.Value);
                }
            }
        }        
    }
}