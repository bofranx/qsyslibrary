﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class SerialEventArgs:EventArgs
    {
        public string @value { get; set; }

        public SerialEventArgs()
        {
        }

        public SerialEventArgs(string v)
        {
            this.@value = v;
        }
    }
}