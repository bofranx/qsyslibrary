﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class QsysEventArgs : EventArgs
    {
        public string ControlName;
        public bool BooleanValue;
        public int IntegerValue;
        public string StringValue;

        public QsysEventArgs(string controlName, bool booleanValue, int integerValue, string stringValue)
        {
            this.ControlName = controlName;
            this.BooleanValue = booleanValue;
            this.IntegerValue = integerValue;
            this.StringValue = stringValue;
        }
    }

    /// <summary>
    /// Used only for internal methods.
    /// </summary>
    public class QsysInternalEventArgs : EventArgs
    {
        public string Name;
        public string String;
        public double Value;
        public double Position;
        public List<string> Choices;

        public QsysInternalEventArgs (string name, string stringValue, double doubleValue, double positionValue)
	    {
            this.Name = name;
            this.String = stringValue;
            this.Value = doubleValue;
            this.Position = positionValue;
	    }

        public QsysInternalEventArgs(string name, string stringValue, double doubleValue, double positionValue, List<string> choices)
        {
            this.Name = name;
            this.String = stringValue;
            this.Value = doubleValue;
            this.Position = positionValue;
            this.Choices = choices;
        }
    }

    /// <summary>
    /// Used only for internal methods.
    /// </summary>
    internal class InternalEvents
    {
        private event EventHandler<QsysInternalEventArgs> onNewEvent = delegate { };

        public event EventHandler<QsysInternalEventArgs> OnNewEvent
        {
            add
            {
                if (!onNewEvent.GetInvocationList().Contains(value))
                {
                    onNewEvent += value;
                }
            }
            remove
            {
                onNewEvent -= value;
            }
        }

        internal void Fire(QsysInternalEventArgs e)
        {
            onNewEvent(null, e);
        }
    }
}