﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public class MethodControlSet : MethodBase
    {
        [JsonProperty("params")]
        public MethodControlSetParams @params { get; set; }

        public MethodControlSet()
        {
            this.id = 2001;
            this.method = "Control.Set";
            this.@params = new MethodControlSetParams();
        }
    }
}