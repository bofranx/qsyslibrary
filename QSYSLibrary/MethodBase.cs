﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class MethodBase
    {
        public int id { get; set; }

        public string jsonrpc = "2.0";

        public string method { get; set; }

        public MethodBase()
        {
        }
    }
}