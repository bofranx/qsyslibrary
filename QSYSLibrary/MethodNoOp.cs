﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class MethodNoOp
    {
        public string jsonrpc = "2.0";
        public string method = "NoOp";
        private object @params = new object { };
    }
}