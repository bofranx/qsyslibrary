﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class AnalogEventArgs
    {
        public ushort @value { get; set; }

        public AnalogEventArgs()
        {
        }

        public AnalogEventArgs(ushort v)
        {
            this.@value = v;
        }
    }
}