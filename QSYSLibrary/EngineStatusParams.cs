﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public class EngineStatusParams
    {
        public string Platform { get; set; }
        public string State { get; set; }
        public string DesignName { get; set; }
        public string DesignCode { get; set; }
        public bool IsRedundant { get; set; }
        public bool IsEmulator { get; set; }
        public MethodStatusGet Status { get; set; }
    }
}