﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public class EngineStatus
    {
        public string jsonrpc { get; set; }
        public int id { get; set; }
        public EngineStatusParams @params { get; set; }
    }
}