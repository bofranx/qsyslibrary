﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class MethodComponentSet : MethodBase
    {
        public MethodComponentSetParams @params { get; set; }

        public MethodComponentSet()
        {
            this.id = 2002;
            this.method = "Component.Set";
            this.@params = new MethodComponentSetParams();
        }

        public MethodComponentSet(string componentName, string controlName, double positionValue)
        {
            this.id = 2002;
            this.method = "Component.Set";

            this.@params = new MethodComponentSetParams();
            this.@params.Name = componentName;

            MethodComponentSetParamsControl control = new MethodComponentSetParamsControl();
            control.Name = controlName;
            control.Position = positionValue;

            this.@params.Controls.Add(control);
        }        
    }
}