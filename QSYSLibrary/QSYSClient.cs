﻿using Crestron.SimplSharp;                          				// For Basic SIMPL# Classes
using Crestron.SimplSharp.CrestronSockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;


namespace QSYSLibrary
{
    public class QSYSClient
    {
        //this is a test to see if git branch works
        private TCPClient tcpClient;
        private TCPClientReceiveCallback OnDataReceiveCallback;
        internal uint _debug;
        internal bool _autoPollControlGroup;
        internal bool _autoPollComponentGroup;
        internal bool _init;
        internal uint _reconnect;
        private string _rxData;
        public Dictionary<string, SoftphoneComponent> _softphoneComponents;
        public Dictionary<string, NamedControl> _namedControls;

        //Dictionaries
        internal Dictionary<Component, InternalEvents> Components = new Dictionary<Component, InternalEvents>();

        //Queues
        private CrestronQueue<string> commandQueue;
        private CrestronQueue<string> responseQueue;

        //Timers
        private CTimer commandQueueTimer;
        private CTimer responseQueueTimer;
        private CTimer reconnectTimer;
        private CTimer keepAlive;

        private JsonSerializerSettings jsonSettings = new JsonSerializerSettings();

        public uint Debug
        {
            get { return _debug; }
            set { _debug = value; }
        }

        public uint Reconnect
        {
            get
            {
                return _reconnect;
            }

            set
            {
                _reconnect = value;
            }
        }

        public delegate void AnalogDelegate(AnalogEventArgs x);
        public delegate void SerialDelegate(SimplSharpString x);
        public delegate void EngineStatusDelegate(object o, EngineStatusEventArgs e);
        public event EngineStatusDelegate OnEngineStatusEvent;

        public AnalogDelegate Socket_Status_ { set; get; }
        public SerialDelegate Rx_ { set; get; }

        private void Dispose()
        {
            this.tcpClient.DisconnectFromServer();
            this.commandQueue.Dispose();
            this.commandQueueTimer.Stop();
            this.commandQueueTimer.Dispose();

            if (!this.keepAlive.Disposed)
            {
                this.keepAlive.Stop();
                this.keepAlive.Dispose();
            }
        }

        public void Initialize(int id, string address)
        {
            if (QSYSMain.AddCore(this, id))
            {   
                this.tcpClient = new TCPClient(address, 1710, 65535);
                this.tcpClient.SocketStatusChange += new TCPClientSocketStatusChangeEventHandler(tcpClient_SocketStatusChange);

                if (this.commandQueue == null)
                    this.commandQueue = new CrestronQueue<string>();

                if (this.responseQueue == null)
                    this.responseQueue = new CrestronQueue<string>();

                if (this.commandQueueTimer == null)
                    this.commandQueueTimer = new CTimer(CommandQueueDequeue, null, 0, 50);

                if (this.responseQueueTimer == null)
                    this.responseQueueTimer = new CTimer(ResponseQueueDequeue, null, 0, 50);
            }
            else
            {
                CrestronConsole.PrintLine("\nQSYSClient Initialize: {0} already found in dictionary", address);
                this.tcpClient = QSYSMain.Devices[id].tcpClient;
            }
            

        }

        public QSYSClient()
        {
            this.jsonSettings.NullValueHandling = NullValueHandling.Ignore;
            this.jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;

            _namedControls = new Dictionary<string, NamedControl>();
            _softphoneComponents = new Dictionary<string, SoftphoneComponent>();
        }


        void tcpClient_SocketStatusChange(TCPClient myTCPClient, SocketStatus clientSocketStatus)
        {
            this.Socket_Status_(new AnalogEventArgs((ushort)clientSocketStatus));
            if (clientSocketStatus == SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                if (this.reconnectTimer != null)
                {
                    this.reconnectTimer.Dispose();                    
                }
            }
            else
            {
                this._init = false;
                //this.Dispose();
                this._autoPollControlGroup = false;
                this._autoPollComponentGroup = false;                
                this._softphoneComponents.Clear();
                this._namedControls.Clear();
                OnEngineStatusEvent(this, new EngineStatusEventArgs(this._init));

                if (reconnectTimer != null) reconnectTimer = new CTimer(_OnReconnectCallback, 5000);
            }
        }

        public void AddNamedComponent(SoftphoneComponent namedComponent, string name)
        {
            try
            {
                this._softphoneComponents.Add(name, namedComponent);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("\nQSYSClient AddNamedComponent exception: ", e.Message);
            }
        }

        public void AddNamedControl(NamedControl namedControl, string name)
        {
            try
            {
                this._namedControls.Add(name, namedControl);
                CrestronConsole.PrintLine("Named control added to dictionary: {0}", name);
            }
            catch(Exception e)
            {
                CrestronConsole.PrintLine("\nQSYSClient AddNamedControl exception: ", e.Message);
            }
        }

        private void CommandQueueDequeue(object _o)
        {
            if (!this.commandQueue.IsEmpty)
            {
                var data = this.commandQueue.Dequeue();

                byte[] bytes = Encoding.ASCII.GetBytes(string.Concat(data, '\0'));
                this.tcpClient.SendData(bytes, bytes.Length);
            }
        }

        public void ConnectToServer()
        {
            SocketErrorCodes returnCode = tcpClient.ConnectToServerAsync(this._OnClientConnectCallback, this);
            reconnectTimer = new CTimer(_OnReconnectCallback, 5000);
            if (_debug > 0)
            {
                CrestronConsole.PrintLine("\nQSYSClient ConnectToServer returnCode: " + returnCode.ToString());
            }
        }

        public void DisconnectFromServer()
        {
            SocketErrorCodes returnCode = tcpClient.DisconnectFromServer();
            if (_debug > 0)
            {
                CrestronConsole.PrintLine("\nQSYSClient DisconnectFromServer returnCode: " + returnCode.ToString());
            }
        }

        private void InitControls()
        {
            if (Components.Count() > 0)
            {
                MethodChangeGroupAddComponent addComponents;
                foreach (var item in Components)
                {
                    addComponents = new MethodChangeGroupAddComponent();
                    addComponents.@params = new MethodChangeGroupAddComponentParams();
                    addComponents.@params.Component = item.Key;
                    commandQueue.Enqueue(JsonConvert.SerializeObject(addComponents));
                }
            }
        }

        public void ParseResponse(string _data)
        {
            try
            {
                this.responseQueue.Enqueue(_data);
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("\nError parsing response from device: {0}", e.Message);
            }
        }

        internal bool RegisterNamedComponent(Component _component)
        {
            try
            {
                lock(Components)
                {                    
                    if (!Components.ContainsKey(_component))
                    {
                        Components.Add(_component, new InternalEvents());
                        CrestronConsole.PrintLine("\nAdding named component {0}", _component.Name);

                        if (this._init)
                        {
                            MethodChangeGroupAddComponent addComponent = new MethodChangeGroupAddComponent();
                            addComponent.@params = new MethodChangeGroupAddComponentParams();
                            addComponent.@params.Component = _component;
                            this.SendData(JsonConvert.SerializeObject(addComponent));
                            CrestronConsole.PrintLine(JsonConvert.SerializeObject(addComponent));
                        }
                        return true;
                    }
                    else
                    {
                        CrestronConsole.PrintLine("\nComponent already in dictionary {0}", _component.Name);
                        return false;
                    }                  
                    
                }
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("\nError adding component {0} to dictionary: {1} / {2}", _component.Name, e.Message);
                return false;
            }
        }


        StringBuilder RxData = new StringBuilder();
        bool busy = false;
        int Pos = -1;
        private void ResponseQueueDequeue(object _o)
        {
            if (!this.responseQueue.IsEmpty)
            {
                try
                {
                    string tmpString = this.responseQueue.Dequeue(); // removes string from queue, blocks until an item is queued

                    this.RxData.Append(tmpString); //Append received data to the COM buffer

                    if (!this.busy)
                    {
                        this.busy = true;
                        while (this.RxData.ToString().Contains("\x00"))
                        {
                            this.Pos = this.RxData.ToString().IndexOf("\x00");
                            var data = this.RxData.ToString().Substring(0, Pos);
                            var garbage = this.RxData.Remove(0, Pos + 1); // remove data from COM buffer

                            this.ParseInternalResponse(data);
                        }

                        this.busy = false;
                    }
                }
                catch (Exception e)
                {
                    this.busy = false;
                    ErrorLog.Exception(e.Message, e);
                }
            }
        }

        public void SendData(string data)
        {
            if(this.tcpClient.ClientStatus != (SocketStatus)2)
            {
                tcpClient.ConnectToServerAsync(this._OnClientConnectCallback, this);
            }
            else
            {
                this.commandQueue.Enqueue(data);
            }
        }

        private void _OnClientConnectCallback(TCPClient client, object o)
        {
            if (client.ClientStatus == SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                this.OnDataReceiveCallback = new TCPClientReceiveCallback(_OnDataReceiveCallback);
                this.tcpClient.ReceiveDataAsync(_OnDataReceiveCallback);
            }
        }

        public void _OnDataReceiveCallback(TCPClient client, int numBytes)
        {
            string str = Encoding.ASCII.GetString(client.IncomingDataBuffer, 0, numBytes);
            this.ParseResponse(str);
            this.tcpClient.ReceiveDataAsync(this._OnDataReceiveCallback);
        }

        private void ParseInternalResponse(string _returnString)
        {
            if (_returnString.Length > 0)
            {
                try
                {
                                JObject jObject = JObject.Parse(_returnString);

                                if (jObject["jsonrpc"] != null)
                                {

                                    if (jObject["error"] != null)
                                    {
                                        ErrorLog.Error("\nQSYSClient _OnDataReceiveCallback error: " + jObject["error"].ToString());
                                    }
                                    else if (jObject["result"] != null)
                                    {
                                        try
                                        {
                                            this.keepAlive.Reset(30000);
                                            if (jObject["id"] != null)
                                            {
                                                int num = jObject["id"].Value<int>();
                                                switch (num)
                                                {
                                                    case 1:  //engine status
                                                        {
                                                            var engineStatus = JsonConvert.DeserializeObject<ResultEngineStatus>(_returnString);

                                                            if (engineStatus.result.State == "Active" ? true : false)
                                                            {
                                                                this._init = true;
                                                            }

                                                            OnEngineStatusEvent(this, new EngineStatusEventArgs(engineStatus.result.State, engineStatus.result.DesignName, engineStatus.result.DesignCode, this._init));
                                                            break;
                                                        }

                                                    case 99:
                                                        {
                                                            _autoPollControlGroup = true;
                                                            break;
                                                        }
                                                    case 100:
                                                        {
                                                            _autoPollComponentGroup = true;
                                                            break;
                                                        }
                                                    case 1001:
                                                        {
                                                            if (!_autoPollControlGroup)
                                                            {
                                                                MethodChangeGroupAutoPoll autoPoll = new MethodChangeGroupAutoPoll();
                                                                this.SendData(JsonConvert.SerializeObject(autoPoll, Formatting.None));
                                                                _autoPollControlGroup = true;
                                                            }
                                                            break;
                                                        }
                                                    case 1002:
                                                        {
                                                            if (!_autoPollComponentGroup)
                                                            {
                                                                MethodChangeGroupAutoPoll autoPoll = new MethodChangeGroupAutoPoll();
                                                                autoPoll.id = 100;
                                                                autoPoll.@params.Id = "ComponentGroup";
                                                                this.SendData(JsonConvert.SerializeObject(autoPoll, Formatting.None));
                                                                _autoPollComponentGroup = true;
                                                            }
                                                            break;
                                                        }
                                                    case 2001:
                                                        {
                                                            //JToken result = (JToken)jObject["result"];
                                                            
                                                            //foreach(var item in this._namedControls)
                                                            //{
                                                            //    if (_namedControls.ContainsKey(result["Name"].ToString()))
                                                            //        this._namedControls[result["Name"].ToString()].QSYSNamedControlResponse(result);
                                                            //}
                                                            break;
                                                        }
                                                }
                                            }
                                        }
                                        catch (Exception e)
                                        {
                                            ErrorLog.Error("QSYSCore error parsing result: " + e.Message);
                                        }
                                    }
                                    else if (jObject["method"] != null)
                                    {
                                        string method = jObject["method"].ToString();
                                        switch (method)
                                        {
                                            case "EngineStatus":
                                                {
                                                    var engineStatus = JsonConvert.DeserializeObject<EngineStatus>(_returnString);

                                                    if (engineStatus.@params.State == "Active" ? true : false)
                                                    {
                                                        this._init = true;
                                                    }

                                                    InitControls();

                                                    //MethodChangeGroupAutoPoll autoPoll = new MethodChangeGroupAutoPoll();
                                                    //autoPoll.id = 100;
                                                    //autoPoll.@params.Id = "ComponentGroup";
                                                    //this.SendData(JsonConvert.SerializeObject(autoPoll, Formatting.None));

                                                    OnEngineStatusEvent(this, new EngineStatusEventArgs(engineStatus.@params.State, engineStatus.@params.DesignName, engineStatus.@params.DesignCode, this._init));

                                                    this.keepAlive = new CTimer(this._OnKeepAliveCallback, null, 0, 15000);
                                                    break;
                                                }
                                            case "ChangeGroup.Poll":
                                                {
                                                    JToken str2 = jObject["params"]["Id"];
                                                    if (str2.Value<string>() == "ComponentGroup")
                                                    {
                                                        JArray changes = JArray.Parse(jObject["params"]["Changes"].ToString());
                                                        foreach (var change in changes)
                                                        {
                                                            ComponentChangeResult changeResult = JsonConvert.DeserializeObject<ComponentChangeResult>(change.ToString(), jsonSettings);

                                                            if (changeResult.Component != null)
                                                            {
                                                                foreach (var item in this.Components)
                                                                {
                                                                    if (item.Key.Name == changeResult.Component)
                                                                    {
                                                                        //CrestronConsole.PrintLine(change.ToString());
                                                                        if (changeResult.Choices != null)
                                                                        {
                                                                            item.Value.Fire(new QsysInternalEventArgs(changeResult.Name, changeResult.String, changeResult.Value, changeResult.Position, changeResult.Choices));
                                                                        }
                                                                        else item.Value.Fire(new QsysInternalEventArgs(changeResult.Name, changeResult.String, changeResult.Value, changeResult.Position));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else if (str2.Value<string>() == "NamedControlGroup")
                                                    {
                                                        JArray changes = JArray.Parse(jObject["params"]["Changes"].ToString());
                                                        foreach (var change in changes)
                                                        {
                                                            if (_namedControls.ContainsKey(change["Name"].Value<string>()))
                                                                this._namedControls[change["Name"].Value<string>()].QSYSNamedControlResponse(change);

                                                        }

                                                    }
                                                    break;
                                                }
                                            default:
                                                break;

                                        }
                                    }
                                }
                            //}
                        //}
                    
                }
                catch (Exception e)
                {
                    ErrorLog.Error("\nQSYSClient _OnDataReceiveCallback Exception: " + e.Message);
                }
            }
        }

        private void _OnKeepAliveCallback(object o)
        {
            MethodStatusGet statusGet = new MethodStatusGet();
            SendData(JsonConvert.SerializeObject(statusGet));
        }

        private void _OnReconnectCallback(object o)
        {
            if (this.tcpClient.ClientStatus != SocketStatus.SOCKET_STATUS_CONNECTED)
            {
                CrestronConsole.PrintLine("\nQSYSClient({0}) attempting to reconnect...", this.tcpClient.AddressClientConnectedTo);
                this.ConnectToServer();
            }
        }
    }
}
