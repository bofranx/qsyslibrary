﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class MethodChangeGroupAddComponent
    {
        public string jsonrpc = "2.0";
        public int id = 1002;
        public string method = "ChangeGroup.AddComponentControl";
        public MethodChangeGroupAddComponentParams @params { get; set; }

        //public MethodChangeGroupAddComponent(string name)
        //{
        //    this.@params = new MethodChangeGroupAddComponentParams();
        //    this.@params.Component.Name = name;
        //}

        //public void AddControl(string controlName)
        //{
        //    try
        //    {
        //        ControlName control = new ControlName();
        //        control.Name = controlName;
        //        this.@params.Component.Controls.Add(control);
        //    }
        //    catch (Exception e)
        //    {
        //        CrestronConsole.PrintLine("QSYSLibrary MethodChangeGroupAddComponent {0} exception: " + e.Message, this.@params.Component.Name);
        //    }
        //}
    }

    public class MethodChangeGroupAddComponentParams
    {
        public string Id = "ComponentGroup";
        public Component Component = new Component();
    }
}