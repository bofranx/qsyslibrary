﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class MethodChangeGroupAutoPoll
    {
        public string jsonrpc = "2.0";
        public int id = 99;
        public string method = "ChangeGroup.AutoPoll";
        public MethodGroupAutoPollParams @params { get; set; }

        public MethodChangeGroupAutoPoll()
        {
            this.@params = new MethodGroupAutoPollParams();
        }

        public class MethodGroupAutoPollParams
        {
            public string Id = "NamedControlGroup";
            public double Rate = 0.15;
        }
    }
}