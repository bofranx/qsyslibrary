﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public class MethodControlSetString : MethodBase
    {
        [JsonProperty("params")]
        public MethodControlSetStringParams @params { get; set; }

        public MethodControlSetString()
        {
            this.id = 2001;
            this.method = "Control.Set";
            this.@params = new MethodControlSetStringParams();
        }

        public class MethodControlSetStringParams
        {
            public string Name { get; set; }
            public string Value { get; set; }

            public MethodControlSetStringParams()
            {

            }
        }
    }
}