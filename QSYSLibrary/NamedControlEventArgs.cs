﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class NamedControlEventArgs : EventArgs
    {
        public string Name { get; set; }
        public string String { get; set; }
        public short Value { get; set; }
        public ushort Position { get; set; }
        public ushort ScaledValue { get; set; }
        public ushort Index { get; set; }
        public string ListItem { get; set; }


        public NamedControlEventArgs()
        {
        }

        public NamedControlEventArgs(ushort index, string listItem)
        {
            this.Index = index;
            this.ListItem = listItem;
        }

        public NamedControlEventArgs(string name, string @string, short @value, ushort position, ushort scaledValue)
        {
            this.Name = name;
            this.String = @string;
            this.Value = @value;
            this.Position = position;
            this.ScaledValue = scaledValue;
            this.Index = 0;
        }

    }
}