﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class MethodChangeGroupAddRemoveControl
    {
        public string jsonrpc = "2.0";

        public int id = 1001;

        public string method = "ChangeGroup.AddControl";

        public Params @params { get; set; }

        public class Params
        {
            public string Id = "NamedControlGroup";
            public List<string> Controls = new List<string>();
        }

        public MethodChangeGroupAddRemoveControl()
        {
            this.@params = new Params();
        }

        public MethodChangeGroupAddRemoveControl(string controlName)
        {
            this.@params = new Params();
            this.@params.Controls.Add(controlName);
        }
    }
}