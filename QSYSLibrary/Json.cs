﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;

namespace QSYSLibrary
{
    public class Component
    {
        public string Name { get; set; }
        public IList<ControlName> Controls { get; set; }
    }

    public class ComponentChangeResult
    {
        public string Component { get; set; }
        public string Name { get; set; }
        public string String { get; set; }
        public double Value { get; set; }
        public double Position { get; set; }
        public List<string> Choices { get; set; }
    }

    public class ControlName
    {
        public string Name { get; set; }

        public ControlName(string name)
        {
            this.Name = name;
        }
    }

    public class MethodComponentSetParams
    {
        public string Name { get; set; }
        public List<Object> Controls = new List<Object>();
    }

    public class MethodComponentSetParamsControl
    {
        public string Name { get; set; }
        public string String { get; set; }
        public decimal? Value { get; set; }
        public double? Position { get; set; }
    }

    public class RecentCallItem
    {
        public string Text { get; set; }
        public string Color { get; set; }
        public string Icon { get; set; }
    }


}