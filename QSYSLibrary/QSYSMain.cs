﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;

namespace QSYSLibrary
{
    public static class QSYSMain
    {

        public static Dictionary<int, QSYSClient> Devices;

        static QSYSMain()
        {
            QSYSMain.Devices = new Dictionary<int, QSYSClient>();
        }

        public static bool AddCore(QSYSClient core, int id)
        {
            bool isNew;
            try
            {
                if (QSYSMain.Devices.ContainsKey(id))
                {
                    isNew = false;
                }
                else
                {
                    QSYSMain.Devices.Add(id, core);
                    isNew = true;
                }
            }
            catch (Exception e)
            {
                CrestronConsole.PrintLine("QSYSMain AddCore exception: " + e.Message);                   
                isNew = false;
            }
            return isNew;
        }
    }
}