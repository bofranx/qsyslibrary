﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Crestron.SimplSharp.CrestronIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;


namespace QSYSLibrary
{
    public class SoftphoneComponent
    {
        Component component = new Component();

        public string componentName;
        internal QSYSClient myClient;
        internal JsonSerializerSettings jsonSettings;

        //delegates
        public delegate void AnalogDelegate(ushort a);
        public delegate void ListDelegate(ushort index, SimplSharpString s);
        public delegate void SerialDelegate(SimplSharpString s);
        public delegate void TestDelegate(SimplSharpString controlName, SimplSharpString stringValue);

        public AnalogDelegate callConnectTime { get; set; }
        public AnalogDelegate callHistoryListCount { get; set; }
        public ListDelegate callHistoryListItem { get; set; }
        public SerialDelegate callCIDName { get; set; }
        public SerialDelegate callCIDNumber { get; set; }       
        public SerialDelegate callNumber { get; set; }
        public SerialDelegate callProgress { get; set; }       
        public TestDelegate newTestData { get; set; }

        //call history
        public ushort listIndex;
        public List<string> historyItemsFormatted = new List<string>();
        public List<string> historyItemsNumbers = new List<string>();

        //component members
        private short _dirValue;
        private ushort _positionValue;
        private ushort _poll;
        private string _stringValue;  

        public short DirValue
        {
            get
            {
                return _dirValue;
            }
            set
            {
                if (myClient != null && _dirValue != value)
                {
                    MethodControlSet controlSet = new MethodControlSet();
                    controlSet.@params.Name = this.componentName;
                    controlSet.@params.Value = (decimal)value;
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._dirValue = value;
                }
            }
        }

        public ushort PositionValue
        {
            get
            {
                return _positionValue;
            }
            set
            {
                if (myClient != null && _positionValue != value)
                {
                    MethodControlSet controlSet = new MethodControlSet();
                    controlSet.@params.Name = this.componentName;
                    controlSet.@params.Position = (double)value / 65535;
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._positionValue = value;
                }
            }
        }

        public ushort Poll
        {
            get
            {
                return _poll;
            }
            set
            {
                if (myClient != null)
                {
                    this._poll = value;
                }

            }
        }

        public string StringValue
        {
            get
            {
                return this._stringValue;
            }
            set
            {
                if (myClient != null && _stringValue != value)
                {
                    MethodControlSetString controlSet = new MethodControlSetString();
                    controlSet.@params.Name = this.componentName;
                    controlSet.@params.Value = value;
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._stringValue = value;
                }
            }
        }

        /// <summary>
        /// Attempt to add this component to the Components dictionary of QSYSClient
        /// </summary>
        /// <param name="name">Component name</param>
        /// <param name="id">Core ID</param>
        public void Register(string name, ushort id)
        {
            this.componentName = name;
            
            component.Name = this.componentName;
            List<ControlName>controls = new List<ControlName>();
            controls.Add(new ControlName("call.cid.name"));
            controls.Add(new ControlName("call.cid.number"));
            controls.Add(new ControlName("call.number"));
            controls.Add(new ControlName("call.status"));
            //controls.Add(new ControlName("call.history"));
            controls.Add(new ControlName("call.connect.time"));
            controls.Add(new ControlName("recent.calls"));
            component.Controls = controls;

            if (QSYSMain.Devices.ContainsKey(id))
            {
                this.myClient = QSYSMain.Devices[id];

                if (this.myClient.RegisterNamedComponent(component))
                {
                    this.myClient.Components[component].OnNewEvent += new EventHandler<QsysInternalEventArgs>(SoftphoneComponent_OnNewEvent);
                }   

            }
            else
            {
                ErrorLog.Error("QSYSLibrary NamedComponent({0}): client is invalid", this.componentName);
            }
        }

        /// <summary>
        /// Receive event from QSYSClient response.  Use e.Name to parse through controls that have been added.
        /// </summary>
        /// <param name="sender">This object</param>
        /// <param name="e">QsysInternalEventArgs</param>
        private void SoftphoneComponent_OnNewEvent(object sender, QsysInternalEventArgs e)
        {
           
            //for testing only
            //if (newTestData != null)
            //    newTestData(e.Name, e.String);

            switch (e.Name)
            {
                case "call.cid.name":
                    callCIDName(e.String);
                    break;
                case "call.cid.number":
                    callCIDNumber(e.String);
                    break;
                case "call.number":
                    callNumber(e.String);                    
                    break;
                case "call.status":
                    callProgress(e.String);
                    break;
                case "call.connect.time":
                    callConnectTime((ushort)e.Value);                    
                    break;
                case "call.history":
                    {
                        ushort i = 0;

                        if (e.String.Length > 0)
                        {
                            string str1 = e.String.Replace("Call ", "Call").Replace("\\", "");
                            JToken obj = JToken.Parse(str1);
                            JArray history = JArray.Parse(obj["CallHistory"].ToString());                            


                            if (callHistoryListCount != null)
                                callHistoryListCount((ushort)history.Count);

                            //clear list if populated and now empty                        
                            if (history.Count <= 0)
                            {
                                foreach (var item in historyItemsFormatted)
                                {
                                    if (i < 500)
                                    {
                                        callHistoryListItem((ushort)(i + 1), new SimplSharpString());
                                        i += 1;
                                    }
                                }
                            }

                            this.historyItemsFormatted.Clear();
                            this.historyItemsNumbers.Clear();

                            i = 0;
                            foreach (var item in history)
                            {                               
                                if (i < 500)
                                {
                                    string name = (string)(item["Name"].ToString().Length > 0 ? item["Name"] : "Unknown");
                                    string number = Helpers.FormatPhoneNumber((string)item["Number"]);
                                    string historyItem = String.Format("{0} ({1})", name, number);

                                    string formattedItem = "";
                                    if (!(bool)item["Incoming"])
                                        formattedItem = Helpers.FormatHTMLTextColor(22, historyItem, "#0000FF");
                                    else if ((bool)item["Answered"])
                                        formattedItem = Helpers.FormatHTMLTextColor(22, historyItem, "#00FF00");
                                    else
                                        formattedItem = Helpers.FormatHTMLTextColor(22, historyItem, "#FF0000");

                                    string date = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds((double)item["Timestamp"]).ToShortDateString();
                                    string time = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds((double)item["Timestamp"]).ToShortTimeString();

                                    formattedItem += "<BR>" + Helpers.FormatHTMLTextColor(18, date + " | " + time, "#c0c0c0");
                                    this.historyItemsFormatted.Add(formattedItem);
                                    this.historyItemsNumbers.Add((string)item["Number"]);

                                    i += 1;

                                }
                                else
                                    break;
                            }

                            //show most recent at top of list
                            this.historyItemsFormatted.Reverse();
                            this.historyItemsNumbers.Reverse();

                            //send to simpl
                            i = 0;
                            foreach (var item in historyItemsFormatted)
                            {
                                this.listIndex = i;
                                callHistoryListItem((ushort)(this.listIndex + 1), (SimplSharpString)this.historyItemsFormatted[this.listIndex]);
                                i += 1;
                            }
                        }
                        break;
                    }
                case "recent.calls":
                    {
                        callHistoryListCount((ushort)e.Choices.Count);
                        if (e.Choices.Count > 0)
                        {
                            int i = 0;
                            historyItemsNumbers.Clear();
                            historyItemsFormatted.Clear();
                            foreach (string choice in e.Choices)
                            {
                                RecentCallItem item = JsonConvert.DeserializeObject<RecentCallItem>(choice);
                                Match m = Regex.Match(item.Text, @"\d+");
                                if (m.Success)
                                {
                                    historyItemsNumbers.Add(m.Value);
                                    historyItemsFormatted.Add(Helpers.FormatPhoneNumber(m.Value));
                                    callHistoryListItem((ushort)(i + 1), (SimplSharpString)this.historyItemsFormatted[i]);
                                    i++;
                                }
                            }
                        }
                        else
                        {
                            historyItemsNumbers.Clear();
                            historyItemsFormatted.Clear();
                        } 
                        break;
                    }


            }
        }

        /// <summary>
        /// Sets 'call.number' control to the selected index of historyItemsNumbers
        /// </summary>
        /// <param name="index">Index of historyItemsNumbers to select</param>
        public void SelectCallHistoryItem(int index)
        {
            if (index > historyItemsFormatted.Count ? false : myClient._init && index > 0)
            {
                index--;
                //SetControlString("call.number", ""); 
                SetControlString("call.number", historyItemsNumbers[index].ToString());
            }
        }

        /// <summary>
        /// Send trigger command of control with controlName
        /// </summary>
        /// <param name="controlName">Name of control within component</param>
        public void Trigger(string controlName)
        {
            if (myClient._init)
            {
                MethodComponentSet componentSet = new MethodComponentSet(this.componentName, controlName, (double)1);
                myClient.SendData(JsonConvert.SerializeObject(componentSet, Formatting.None, jsonSettings));
            }
        }

        /// <summary>
        /// Sets string value of a control.
        /// </summary>
        /// <param name="controlName">Name of control within component</param>
        /// <param name="value">Value to set</param>
        public void SetControlString(string controlName, string value)
        {
            if (myClient._init)
            {
                MethodComponentSet componentSet = new MethodComponentSet();
                componentSet.@params.Name = this.componentName;
                var obj = new { Name = controlName, Value = value };
                componentSet.@params.Controls.Add(obj);
                myClient.SendData(JsonConvert.SerializeObject(componentSet, Formatting.None, jsonSettings));
            }
        }

        public SoftphoneComponent()
        {
            this.jsonSettings = new JsonSerializerSettings();
            jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            jsonSettings.NullValueHandling = NullValueHandling.Ignore;
        }
    }
}