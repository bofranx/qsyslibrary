﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Crestron.SimplSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace QSYSLibrary
{
    public class NamedControl
    {
        public string controlName;
        internal QSYSClient myClient;
        internal CCriticalSection myCC;
        internal JsonSerializerSettings jsonSettings;

        public delegate void ListItemCount(ushort count);
        public delegate void NamedControlEvent(object sender, NamedControlEventArgs args);
        public event NamedControlEvent myNamedControlEvent;
                
        public short ramp;
        private short _dirValue;
        private ushort _scaledValue;
        private ushort _positionValue;
        private ushort _poll;
        private string _stringValue;
        public ListItemCount listCount { get; set; }
        public ushort listIndex;
        public List<string> listItems = new List<string>();

        public short DirValue
        {
            get
            {
                return _dirValue;
            }
            set
            {
                if (myClient != null && _dirValue != value)
                {
                    MethodControlSet controlSet = new MethodControlSet();
                    controlSet.@params.Name = this.controlName;
                    controlSet.@params.Value = (decimal)value;
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._dirValue = value;
                }
            }
        }

        public ushort PositionValue
        {
            get
            {
                return _positionValue;
            }
            set
            {
                if (myClient != null && _positionValue != value)
                {
                    MethodControlSet controlSet = new MethodControlSet();
                    controlSet.@params.Name = this.controlName;
                    controlSet.@params.Position = (double)value / 65535.00;
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._positionValue = value;
                }
            }
        }

        public ushort Poll
        {
            get
            {
                return _poll;
            }
            set
            {
                if (myClient != null)
                {
                    MethodChangeGroupAddRemoveControl addRemove = new MethodChangeGroupAddRemoveControl(this.controlName);
                    if (value <= 0)
                    {
                        addRemove.method = "ChangeGroup.Remove";
                    }
                    myClient.SendData(JsonConvert.SerializeObject(addRemove, Formatting.None, jsonSettings));

                    this._poll = value;
                }

            }
        }

        public ushort ScaledValue
        {
            get
            {
                return _scaledValue;
            }
            set
            {
                if (myClient != null && _scaledValue != value)
                {
                    MethodControlSet controlSet = new MethodControlSet();
                    controlSet.@params.Name = this.controlName;
                    controlSet.@params.Value = Helpers.logScaleSetValue((double)value / 65535.00);
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._scaledValue = value;
                }
            }
        }

        public string StringValue
        {
            get
            {
                return this._stringValue;
            }
            set
            {
                if(myClient != null && _stringValue != value)
                {
                    MethodControlSetString controlSet = new MethodControlSetString();
                    controlSet.@params.Name = this.controlName;
                    controlSet.@params.Value = value;
                    this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));

                    this._stringValue = value;
                }
            }
        }

        public void Register(string name, int id)
        {
            this.controlName = name;
            if(QSYSMain.Devices.ContainsKey(id)) 
            {
                myClient = QSYSMain.Devices[id];
                myCC = new CCriticalSection();
                int index = myClient._namedControls.FindIndex(x => x.controlName == this.controlName);
                if (index >= 0) myClient._namedControls[index] = this;
                else
                {
                    myCC.Enter();
                    {
                        CrestronConsole.PrintLine("QSYSLibrary NamedControl({0}): attemping to register named control", this.controlName);
                        QSYSMain.Devices[id].AddNamedControl(this, name);
                        //MethodChangeGroupAddRemoveControl addControl = new MethodChangeGroupAddRemoveControl(this.controlName);
                        //QSYSMain.Devices[id].SendData(JsonConvert.SerializeObject(addControl, Formatting.None, jsonSettings));
                    }
                    myCC.Leave();
                }
            }
            else
            {
                CrestronConsole.PrintLine("QSYSLibrary NamedControl({0}): client is invalid", this.controlName);
            }
        }

        public void QSYSNamedControlResponse(JToken jToken)
        {
            this.controlName = jToken["Name"].Value<string>();
            this._stringValue = jToken["String"].Value<string>();
            this._dirValue = (short)jToken["Value"].Value<short>();
            this._positionValue = Convert.ToUInt16((float)((float)jToken["Position"]) * 100f * 655.35);
            this._scaledValue = Helpers.logScaleReturnValue((double)jToken["Value"]);

            if (jToken["Choices"] != null)
            {
                JArray choices = JArray.Parse(jToken["Choices"].ToString());
                ushort i = 0;
                if (choices.Count <= 0)
                {
                    this.listIndex = i;
                    if (this.listItems.Count > 0)
                    {
                        for (i = 0; i < this.listItems.Count(); i++)
                        {
                            myNamedControlEvent(this, new NamedControlEventArgs((ushort)(i + 1), ""));
                        }
                    }
                    this.listItems.Clear();
                }
                else
                {
                    this.listItems.Clear();
                    this.listCount((ushort)choices.Count);
                    foreach (var choice in choices)
                    {
                        JObject obj = JObject.Parse(choice.Value<string>().Replace("\\", ""));
                        if (i < 500)
                        {
                            this.listIndex = i;
                            //this.listItems.Add(choice.Value<string>());
                            this.listItems.Add((string)obj["Text"]);
                            myNamedControlEvent(this, new NamedControlEventArgs((ushort)(this.listIndex + 1), this.listItems[i]));
                            i += 1;
                        }
                        else
                            break;
                    }
                }
            }
            else
            {
                myNamedControlEvent(this, new NamedControlEventArgs(this.controlName, this.StringValue, this.DirValue, this.PositionValue, this.ScaledValue));
            }
        }

        public void SelectListItem(int index)
        {
            if (index > listItems.Count ? false : myClient._init && index > 0)
            {
                index--;
                MethodControlSetString controlSet = new MethodControlSetString();
                controlSet.@params.Name = controlName;
                controlSet.@params.Value = listItems[index];

                this.myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));
            }
        }

        public void Trigger()
        {
            if (myClient._init)
            {
                MethodControlSet controlSet = new MethodControlSet();
                controlSet.@params.Name = this.controlName;
                controlSet.@params.Position = 1;

                myClient.SendData(JsonConvert.SerializeObject(controlSet, Formatting.None, jsonSettings));
            }
        }

        public NamedControl()
        {
            this.jsonSettings = new JsonSerializerSettings();
            jsonSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            jsonSettings.NullValueHandling = NullValueHandling.Ignore;
        }

    }
}